// Soal 1
function range(startNum, finishNum){
    if(startNum == undefined || finishNum == undefined){
        return -1;
    }
    else{
        var result = [];
        
        if(startNum <= finishNum){
            for(var i = startNum; i <= finishNum; i ++) result.push(i);
            return result;
        }
        else{
            for(var i = startNum; i >= finishNum; i --) result.push(i);
            return result;
        }
    }
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Soal 2
function rangeWithStep(startNum, finishNum, step){
    if(startNum == undefined || finishNum == undefined || step == undefined){
        return -1;
    }
    else{
        var result = [];
        
        if(startNum <= finishNum){
            for(var i = startNum; i <= finishNum; i += step) result.push(i);
            return result;
        }
        else{
            for(var i = startNum; i >= finishNum; i -= step) result.push(i);
            return result;
        }
    }
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// Soal 3
function sum(startNum = 0, finishNum = startNum, step = 1){
    var result = 0;
    
    if(startNum <= finishNum){
        for(var i = startNum; i <= finishNum; i += step) result += i;
        return result;
    }
    else{
        for(var i = startNum; i >= finishNum; i -= step) result += i;
        return result;
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0

// Soal 4
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(array){
    for(var i = 0; i < array.length; i++) {
        console.log("Nomor ID: ", array[i][0]);
        console.log("Nama Lengkap: ", array[i][1]);
        console.log("TTL: ", array[i][2], array[i][3]);
        console.log("Hobi: ", array[i][4]);
        console.log("");
    }
}

dataHandling(input);

// Soal 5
function balikKata(string){
    reversed = "";
    for (var i = string.length - 1; i >= 0; i--){
        reversed += string[i];
    }
    return reversed;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

function dataHandling2(input){
    // Output 1
    input[1] = input[1] + "Elsharawy";
    input[2] = "Provinsi " + input[2];
    input.splice(4, 2, "Pria", "SMA Internasional Metro");
    console.log(input);

    // Output 2
    var date = input[3].split('/');
    var strBulan;
    switch (date[1]) {
        case "01":
            strBulan = 'Januari';
            break;
        case "02":
            strBulan = 'Februari';
            break;
        case "03":
            strBulan = 'Maret';
            break;
        case "04":
            strBulan = 'April';
            break;
        case "05":
            strBulan = 'Mei';
            break;
        case "06":
            strBulan = 'Juni';
            break;
        case "07":
            strBulan = 'Juli';
            break;
        case "08":
            strBulan = 'Agustus';
            break;
        case "09":
            strBulan = 'September';
            break;
        case "10":
            strBulan = 'Oktober';
            break;
        case "11":
            strBulan = 'November';
            break;
        case "12":
            strBulan = 'Desember';
            break;
        default:
            break;
    }

    console.log(strBulan);

    // Output 3
    console.log(date.slice().sort(function(str1, str2){
        return -(parseInt(str1) - parseInt(str2));
    }));

    // Output 4
    console.log(date.join('-'));

    // Output 5
    console.log(input[1].slice(0, 14));
}

dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 