// Soal 1
var index = 2;
console.log("LOOPING PERTAMA")
while(index <= 20){
    console.log(index + " - I love coding");
    index+=2;
}

console.log("LOOPING KEDUA")
while(index >= 2){
    console.log(index + " - I will become a mobile developer");
    index-=2;
}

// Soal 2
for(var index = 1; index <= 20; index++){
    if(index % 2 == 0){
        console.log(index + " - Berkualitas");
    }
    else{
        if(index % 3 == 0){
            console.log(index + " - I Love Coding");
        }
        else{
            console.log(index + " - Santai");
        }
    }
}

// Soal 3
for(var l = 0; l < 4; l++){
    var sharp = "";
    for(var p = 0; p < 8; p++){
        sharp += "#";
    }
    console.log(sharp);
}

// Soal 4
for(var l = 0; l < 7; l++){
    var sharp = "";
    for(var p = 0; p <= l; p++){
        sharp += "#";
    }
    console.log(sharp);
}

// Soal 5
for(var l = 0; l < 8; l++){
    var sharp = "";
    for(var p = 0; p < 8; p++){
        if((p+l) % 2 == 0) sharp += " ";
        else sharp += "#";
    }
    console.log(sharp);
}