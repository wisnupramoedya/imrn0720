// Soal 1
function balikString(str){
    var result = '';
    for (var i = str.length - 1; i >= 0; i--){
        result += str[i];
    }
    return result;
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

// Soal 2
function palindrome(str){
    for(var i = 0; i < str.length; i++){
        if(str[i] != str[str.length - 1 - i]) return false;
    }
    return true;
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

// Soal 3
function bandingkan(number1, number2){
    if(number2 == undefined){
        if(number1 == undefined){
            return -1;
        }
        else{
            return parseInt(number1);
        }
    }
    if(number1 == number2 || number1 < 0 || number2 < 0) return -1;
    else return (parseInt(number1) > parseInt(number2))? parseInt(number1) : parseInt(number2);
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

// Soal 4
function DescendingTen(number){
    var result = '';
    if(number == undefined) return '-1';

    for(var i = 0; i < 10; i++){
        result += String(number - i) + ' ';
    }
    return result;
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
