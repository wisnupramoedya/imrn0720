import React from 'react';
import { StyleSheet, View, Dimensions, StatusBar } from 'react-native';
import AppIndex from "./Tugas/Tugas15/index";
import Index from './Tugas/Tugas15/TugasNavigation/index'
import AppTask from './Tugas/Quiz3/index'

const statusBarHeight = StatusBar.currentHeight;

export default function App() {
  return (
    <View style={styles.container}>
      <AppTask />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: statusBarHeight,
  },
});
