import React, { Component } from 'react'
import { 
    Text,
    View,
    StyleSheet,
    ScrollView
 } from 'react-native'
import { MaterialIcons, FontAwesome5 } from '@expo/vector-icons'


export class AboutScreen extends Component {
    render() {
        return (
            <ScrollView contentContainerStyle={styles.container}>
                <Text style={[styles.text, styles.textColor, {fontSize: 36, lineHeight: 42}]}>Tentang Saya</Text>
                <MaterialIcons name='account-circle' size={200} color="#EFEFEF" style={{marginTop: 12}} />
                <Text style={[styles.text, styles.textColor, {marginTop: 24, fontSize: 24, lineHeight: 28}]}>Wisnu Pram</Text>
                <Text style={[styles.text, styles.textNormal, {color: '#3EC6FF', marginTop: 8}]}>React Native Developer</Text>
                <View style={[styles.boxColor, {width: 359, height: 140, marginTop: 16, paddingHorizontal: 8}]}>
                    <Text style={[styles.text, styles.textColor, styles.textNormal, {marginVertical: 5}]} >Portofolio</Text>
                    <View style={styles.stroke} />
                    <View style={{flex: 1, justifyContent: "space-around", flexDirection: 'row', alignItems: 'center'}}>
                        <View style={styles.portofolioContent}>
                            <FontAwesome5 name="gitlab" size={42} color="#3EC6FF"/>
                            <Text style={[styles.text, styles.textColor, styles.textNormal, {marginTop: 11}]}>@wisnupram</Text>
                        </View>
                        <View style={styles.portofolioContent}>
                            <FontAwesome5 name="github" size={42} color="#3EC6FF"/>
                            <Text style={[styles.text, styles.textColor, styles.textNormal, {marginTop: 11}]}>@wisnupram</Text>
                        </View>
                    </View>
                </View>
                <View style={[styles.boxColor, {width: 359, height: 251, marginTop: 9, paddingHorizontal: 8}]}>
                    <Text style={[styles.text, styles.textColor, styles.textNormal, {marginVertical: 5}]} >Hubungi Saya</Text>
                    <View style={styles.stroke} />
                    <View style={{flex: 1, justifyContent: "space-around", flexDirection: 'column', alignItems: 'center'}}>
                        <View style={styles.callContent}>
                            <FontAwesome5 name="facebook" size={40} color="#3EC6FF"/>
                            <Text style={[styles.text, styles.textColor, styles.textNormal, {marginLeft: 19}]}>@wisnupram</Text>
                        </View>
                        <View style={styles.callContent}>
                            <FontAwesome5 name="instagram" size={40} color="#3EC6FF"/>
                            <Text style={[styles.text, styles.textColor, styles.textNormal, {marginLeft: 19}]}>@wisnupram</Text>
                        </View>
                        <View style={styles.callContent}>
                            <FontAwesome5 name="twitter" size={40} color="#3EC6FF"/>
                            <Text style={[styles.text, styles.textColor, styles.textNormal, {marginLeft: 19}]}>@wisnupram</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        paddingHorizontal: 8,
        paddingVertical: 64
    },
    text: {
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold'
    },
    textNormal: {
        fontSize: 16,
        lineHeight: 19
    },
    textBox: {
        fontSize: 18,
        lineHeight: 21,
    },
    textColor: {
        color: '#003366'
    },
    boxColor: {
        borderRadius: 16,
        backgroundColor: '#EFEFEF'
    },
    stroke: {
        borderBottomColor: '#003366',
        borderBottomWidth: 1
    },
    portofolioContent: {
        flexDirection: 'column',
        alignItems: "center"
    },
    callContent: {
        flexDirection: 'row',
        alignItems: "center"
    }
})

export default AboutScreen
