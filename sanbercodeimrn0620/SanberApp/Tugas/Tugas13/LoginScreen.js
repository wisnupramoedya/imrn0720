import React, { Component } from 'react'
import { 
    Text,
    View,
    Image,
    StyleSheet,
    TextInput,
    TouchableOpacity
 } from 'react-native'

export class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style={styles.logoImage} />
                <Text style={[styles.login, styles.text, {color: '#003366', marginTop: 56}]}>
                    {"Login"}
                </Text>
                <View style={[styles.formGroup, {marginTop: 40}]}>
                    <Text style={[styles.textForm, styles.text]} >
                        {"Username / Email"}
                    </Text>
                    <TextInput style={styles.inputForm} />
                </View>
                <View style={[styles.formGroup, {marginTop: 16}]}>
                    <Text style={[styles.textForm, styles.text]} >
                        {"Password"}
                    </Text>
                    <TextInput style={styles.inputForm} />
                </View>
                <TouchableOpacity style={[styles.button, {marginTop: 32, backgroundColor: '#3EC6FF'}]}>
                    <Text style={styles.buttonText}>
                        {"Masuk"}
                    </Text>
                </TouchableOpacity>
                <Text style={[styles.login, styles.text, {margin: 16, color: '#3EC6FF'}]}>
                    {"atau"}
                </Text>
                <TouchableOpacity style={[styles.button, {backgroundColor: '#003366'}]}>
                    <Text style={styles.buttonText}>
                        {"Daftar ?"}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
    },
    logoImage: {
        marginTop: 63,
        width: 375,
        height: 116
    },
    text:{
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontStyle: 'normal',
    },
    login: {
        fontSize: 24,
        lineHeight: 28,
    },
    formGroup: {
        width: 294,
        height: 71,
    },
    textForm: {
        color:  '#003366',
        fontSize: 16,
        lineHeight: 19,
    },
    inputForm: {
        flex: 1,
        borderColor: '#003366',
        borderWidth: 1
    },
    button: {
        width: 140,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 16
    },
    buttonText: {
        fontSize: 24,
        lineHeight: 28,
        color: "#FFFFFF"
    }

})

export default LoginScreen
