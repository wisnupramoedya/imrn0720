import React from 'react';
import { View, Text, ScrollView, Image, TextInput, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import skillData from './skillData.json'
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons'

const Content = (props) => {
  return (
    <View style={styles.content}>
      <MaterialCommunityIcons name={props.product.iconName} size={75} color='#003366' />
      
      <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginLeft: 23}}>
        <View>
          <Text style={[styles.font, styles.boldFont, styles.skillFont, {color: blueDark, marginBottom: 5}]}>{props.product.skillName}</Text>
          <Text style={[styles.font, styles.boldFont, styles.mediumFont, {color: blue}]}>{props.product.categoryName}</Text>
          <View style={{alignItems: 'flex-end'}}>
            <Text style={[styles.font, styles.boldFont, styles.numberFont]}>{props.product.percentageProgress}</Text>
          </View>
        </View>

        <MaterialIcons name="navigate-next" size={80} color="#003366" />
      </View>
    </View>
  )
}

export default class SkillScreen extends React.Component {
  render(){
    return (
      <View style={styles.container}>
        <Image source={require('./images/logo.png')} style={styles.logoIcon}/>

        <View style={styles.accountInfo}>
          <MaterialCommunityIcons name="account-circle" size={26} color="#B4E9FF" style={styles.accountLogo}/>
          <View>
            <Text style={[styles.font, styles.smallFont, {color: black}]} >Hai,</Text>
            <Text style={[styles.font, styles.mediumFont, {color: blueDark}]} >Wisnu Pram</Text>
          </View>
        </View>

        <View style={styles.body}>
          <Text style={[styles.font, styles.titleFont, {color: blueDark}]}>
            SKILL
          </Text>
          <View style={styles.stroke} />

          <View style={styles.listLibrary} >
            <View style={styles.library} >
              <Text style={[styles.font, styles.smallFont, styles.boldFont]}>
                Library / Framework
              </Text>
            </View>

            <View style={styles.library} >
              <Text style={[styles.font, styles.smallFont, styles.boldFont]}>
                Bahasa Pemrograman
              </Text>
            </View>

            <View style={styles.library} >
              <Text style={[styles.font, styles.smallFont, styles.boldFont]}>
                Teknologi
              </Text>
            </View>
          </View>

          <FlatList style={{marginBottom: 200}}
            data={skillData.items}
            renderItem={({item}) => <Content product={item} /> }
            keyExtractor={(item) => item.id.toString()}
            ItemSeparatorComponent={() => <View style={{height: 8}} />}
          />
        </View>

      </View>
    )
  }
};

const black = '#000000'
const blueDark = '#003366'
const blue = '#3EC6FF'
const blueLight = '#B4E9FF'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logoIcon: {
    position: 'absolute',
    width: 187.5,
    height: 54,
    top: 0,
    right: 0
  },
  accountInfo: {
    marginTop: 54,
    marginLeft: 16,
    flexDirection: 'row'
  },
  accountLogo: {
    marginTop: 4,
    marginLeft: 3,
    marginBottom: 3,
    marginRight: 11
  },
  font: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
  },
  boldFont: {
    fontWeight: 'bold'
  },
  smallFont: {
    fontSize: 12,
    lineHeight: 14
  },
  mediumFont: {
    fontSize: 16,
    lineHeight: 19
  },
  skillFont: {
    fontSize: 24,
    lineHeight: 28
  },
  titleFont: {
    fontSize: 36,
    lineHeight: 42
  },
  numberFont: {
    color: 'white',
    fontSize: 48,
    lineHeight: 56
  },
  stroke: {
    borderBottomColor: blue,
    borderBottomWidth: 4
  },
  body: {
    marginTop: 16,
    marginHorizontal: 16,
  },
  listLibrary: {
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  library: {
    borderRadius: 8,
    backgroundColor: blueLight,
    paddingHorizontal: 8,
    paddingVertical: 9
  },
  content: {
    width: 343,
    height: 129,
    backgroundColor: blueLight,
    paddingTop: 13,
    paddingBottom: 8,
    paddingHorizontal: 8,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8
  },
  
});

