// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var time = 8000;
var i = 0;
readBooks(time, books[i], function callback(timeRemain){
    if(timeRemain != time){
        i++;
        if(i < books.length) 
            readBooks(timeRemain, books[i], callback);
    }
});