var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var time = 10000;
var i = 0;

function start(timeSign){
    readBooksPromise(timeSign, books[i])
        .then(function(fulfilled){
            i++;
            start(fulfilled);
        })
        .catch(function(error){

        })
}

start(time);