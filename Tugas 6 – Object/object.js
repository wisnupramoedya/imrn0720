// Soal 1
function arrayToObject(arr) {
    // Code di sini 
    var now = new Date();
    var thisYear = now.getFullYear();

    for (var i = 0; i < arr.length; i++){
        var object = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: (typeof(arr[i][3]) == "number" && thisYear - arr[i][3] >= 0)? (thisYear - arr[i][3]) : "Invalid Birth Year"
        };

        // console.log(String(i + 1).concat(". ", object.firstName, " ", object.lastName, ": ", JSON.stringify(object)));
        console.log(String(i + 1).concat(". ", object.firstName, " ", object.lastName, ": "), object);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Soal 2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId)
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    else{
        var objectProduct = {
            'Sepatu Stacattu': 1500000,
            'Baju Zoro': 500000,
            'Baju H&N': 250000,
            'Sweater Uniklooh': 175000,
            'Casing Handphone': 50000
        }

        var listPurchased = [];
        var changeMoney = money;

        for(var i in objectProduct){
            if(changeMoney - objectProduct[i] < 0)
                continue;
            else{
                listPurchased.push(i);
                changeMoney -= objectProduct[i];
            }
        }

        if(listPurchased.length == 0) return "Mohon maaf, uang tidak cukup";

        var objectResult = {};
        objectResult.memberId = memberId;
        objectResult.money = money;
        objectResult.listPurchased = listPurchased;
        objectResult.changeMoney = changeMoney;

        return objectResult;
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    var listPassenger = [];
    for(var i = 0; i < arrPenumpang.length; i++){
        var objectPassenger = {};
        
        function pay(start, end){
            var boolean = false;
            var price = 0;
            for(var j = 0; j < rute.length; j++){
                if(rute[j] == start) boolean = true;
                if(rute[j] == end) boolean = false;          

                if(boolean == true) price += 2000;

            }
            return price;
        }

        objectPassenger.penumpang = arrPenumpang[i][0],
        objectPassenger.naikDari = arrPenumpang[i][1],
        objectPassenger.tujuan = arrPenumpang[i][2],
        objectPassenger.bayar = pay(arrPenumpang[i][1], arrPenumpang[i][2]);

        listPassenger.push(objectPassenger);
    }
    return listPassenger;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]