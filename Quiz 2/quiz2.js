/* 
  SOAL CLASS SCORE (15 poin + 5 es6)
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number atau array of number.
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
// tuliskan coding disini

class Score {
    constructor(subject, points, email){
        this.subject = subject;
        this.points = points;
        this.email = email;
    }

    average() {
        if(Array.isArray(this.points)){
            let sum = 0;
            for (let point of this.points) {
                sum += point;
            }
            return sum/this.points.length;
        }
        else{
            return this.points;
        }
    }
}

let scoreOne = new Score("Math", [100, 80, 90, 85, 95], "wisnupramoedya@gmail.com");
console.log(`Result of ${scoreOne.subject} from ${scoreOne.email} is ${scoreOne.average()}`);
let scoreTwo = new Score("Chemistry", 100, "wisnupramoedya@gmail.com");
console.log(`Result of ${scoreTwo.subject} from ${scoreTwo.email} is ${scoreTwo.average()}`);

/* 
  SOAL View Score (15 Poin + 5 Poin ES6)
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 
  Input 

  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]

  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/
//tuliskan coding disini
var viewScores = (data, subject) => {
    let pointKey;
    for(let subjectKey of Object.keys(data[0])){
        if(data[0][subjectKey] == subject){
            pointKey = subjectKey;
        }
    }
    
    let output = [];
    for(let i = 1; i < data.length; i++){
        let dataOutput = {};
        dataOutput.email = data[i][0];
        dataOutput.subject = subject;
        dataOutput.points = data[i][pointKey];
        output.push(dataOutput);
    }

    console.log(output);
}

var data = [
            ["email", "quiz-1", "quiz-2", "quiz-3"],
            ["abduh@mail.com", 78, 89, 90],
            ["khairun@mail.com", 95, 85, 88]
        ]

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/* 
  SOAL Recap Score (20 Poin + 5 Poin ES6)
    buatlah function recapScore menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    Predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    contoh output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/
//tuliskan code anda disini 
var recapScores = (data) => {
    for(let key of Object.keys(data)){
        let sum = 0;
        for(let i = 1; i < data[key].length; i++){
            sum += data[key][i];
        }
        let averageScore = sum / (data[key].length - 1);
        let predicate;
        if(averageScore > 90) predicate = "honour";
        else if(averageScore > 80) predicate = "graduate";
        else if(averageScore > 70) predicate = "participant";

        let i = Number(key) + 1;
        console.log(`${i}. Email: ${data[key][0]}\nRata-rata: ${averageScore}\nPredikat: ${predicate}\n`)
    }
}

var data = [
    ["wisnu@gmail.com", 100, 80, 90, 95],
    ["ardi@gmail.com", 100, 80]
]

recapScores(data)
