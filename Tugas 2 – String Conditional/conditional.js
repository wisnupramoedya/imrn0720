// Soal 1
var nama = '';
var peran = 'Guard';

if(nama == ''){
    console.log('Nama harus diisi!');
}
else if(peran == ''){
    console.log('Halo '.concat(nama, ', Pilih peranmu untuk memulai game!'));
}
else{
    console.log('Selamat datang di Dunia Werewolf, '.concat(nama));
    if(peran == 'Penyihir'){
        console.log('Halo Penyihir '.concat(nama,', kamu dapat melihat siapa yang menjadi werewolf!'));
    }
    else if(peran == 'Guard'){
        console.log('Halo Guard '.concat(nama,', kamu akan membantu melindungi temanmu dari serangan werewolf.'));
    }
    else if(peran == 'Werewolf'){
        console.log('Halo Werewolf '.concat(nama,', Kamu akan memakan mangsa setiap malam!'));
    }
}

// Soal 2
var tanggal = 17; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 8; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1945; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

var strBulan;
switch (bulan) {
    case 1:
        strBulan = 'Januari';
        break;
    case 2:
        strBulan = 'Februari';
        break;
    case 3:
        strBulan = 'Maret';
        break;
    case 4:
        strBulan = 'April';
        break;
    case 5:
        strBulan = 'Mei';
        break;
    case 6:
        strBulan = 'Juni';
        break;
    case 7:
        strBulan = 'Juli';
        break;
    case 8:
        strBulan = 'Agustus';
        break;
    case 9:
        strBulan = 'September';
        break;
    case 10:
        strBulan = 'Oktober';
        break;
    case 11:
        strBulan = 'November';
        break;
    case 12:
        strBulan = 'Desember';
        break;
    default:
        break;
}

console.log(String(tanggal).concat(' ', strBulan, ' ', String(tahun)));